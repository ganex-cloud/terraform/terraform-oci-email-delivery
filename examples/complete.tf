module "oci_email_sender-teste2-example-com" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-email-delivery.git?ref=master"
  compartment_id = module.compartiment_infra.compartment_id
  email_address  = "teste2@example.com"
}
