output "email_address" {
  value = oci_email_sender.this.email_address
}

output "email_id" {
  value = oci_email_sender.this.id
}
