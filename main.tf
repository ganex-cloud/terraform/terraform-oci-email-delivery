resource "oci_email_sender" "this" {
  compartment_id = var.compartment_id
  email_address  = var.email_address
  defined_tags   = var.defined_tags
  freeform_tags  = var.freeform_tags
}
